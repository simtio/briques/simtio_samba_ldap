#!/usr/bin/env bash

# Get out if variable is not initialized
set -euo pipefail

############
## CONFIG ##
############
echo "
--------------------------------------------
  ___   ___   __  __   _____   ___    ___
 / __| |_ _| |  \/  | |_   _| |_ _|  / _ \
 \__ \  | |  | |\/| |   | |    | |  | (_) |
 |___/ |___| |_|  |_|   |_|   |___|  \___/

--------------------------------------------

Brought to you by SIMTIO
https://www.simtio.fr
--------------------------------------------
  ____    _    __  __ ____    _
 / ___|  / \  |  \/  | __ )  / \
 \___ \ / _ \ | |\/| |  _ \ / _ \
  ___) / ___ \| |  | | |_) / ___ \
 |____/_/   \_\_|  |_|____/_/   \_\

--------------------------------------------"

SECRET="/var/lib/samba/private/secrets.tdb"
SMBCONF="/etc/samba/smb.conf"
AUTHFILE="/etc/libnss-ldap.conf"

# Copy default
cp -v /smb.conf.template ${SMBCONF}
cp -v /libnss-ldap.conf.template ${AUTHFILE}

# Edit ${SMBCONF}
sed -i "s|<LDAP_URI>|${LDAP_URI}|g" ${SMBCONF}
sed -i "s|<LDAP_ADMIN_CN>|${LDAP_ADMIN_CN}|g" ${SMBCONF}
sed -i "s|<LDAP_BASE_SUFFIX>|${LDAP_BASE_SUFFIX}|g" ${SMBCONF}

# Edit ${AUTHFILE}
sed -i "s|<LDAP_URI>|${LDAP_URI}|g" ${AUTHFILE}
sed -i "s|<LDAP_ADMIN_CN>|${LDAP_ADMIN_CN}|g" ${AUTHFILE}
sed -i "s|<LDAP_BASE_SUFFIX>|${LDAP_BASE_SUFFIX}|g" ${AUTHFILE}
sed -i "s|<LDAP_ADMIN_PWD>|${LDAP_ADMIN_PWD}|g" ${AUTHFILE}

# Startup
if [[ -z "${LDAP_ADMIN_PWD}" ]]; then
    echo "ERROR: Environment variable LDAP_ADMIN_PWD not set"
    exit 17
else
    smbpasswd -w ${LDAP_ADMIN_PWD}
fi
if [[ ! -e ${SECRET} ]] ; then
    echo "ERROR: ${SECRET} does not exists"
    exit 15
fi

if [[ $# -ge 1 && -x $(which $1 2>&-) ]]; then
    exec "$@"
elif [[ $# -ge 1 ]]; then
    echo "ERROR: command not found: $1"
    exit 13
elif ps -ef | egrep -v grep | grep -q smbd; then
    echo "Service already running, please restart container to apply changes"
else
    [[ ${NMBD:-""} ]] && ionice -c 3 nmbd -D
    exec ionice -c 3 smbd -FS </dev/null
fi
