# SIMTIO - Samba-LDAP

Docker image for SAMBA with ldap authentication.

## VARS
- LDAP_URI = Full LDAP URI, (e.g. `ldap://192.168.1.1`)
- LDAP_BASE_SUFFIX = DC base suffix (e.g. `dc=base`)
- LDAP_ADMIN_CN = CN of LDAP Admin (e.g. `cn=admin,dc=base`)
- LDAP_ADMIN_PWD = Password of LDAP Admin

## Mountpoints :
- /var/lib/samba
- /var/log/samba

## Dev
```
docker build -t registry.gitlab.com/simtio/briques/simtio_samba_ldap:latest -t registry.gitlab.com/simtio/briques/simtio_samba_ldap:0.x .

docker run -p 445:445 --rm --name simtio_samba_ldap \
-e LDAP_URI=ldap://localhost -e LDAP_BASE_SUFFIX='dc=base' -e LDAP_ADMIN_CN='cn=admin,dc=base' -e LDAP_ADMIN_PWD=test registry.gitlab.com/simtio/briques/simtio_samba_ldap:latest

docker push registry.gitlab.com/simtio/briques/simtio_samba_ldap && docker push registry.gitlab.com/simtio/briques/simtio_samba_ldap:0.x
```

Multiarch :
* `docker buildx build -t simtio/samba_ldap:latest -t simtio/samba_ldap:0.x --platform=linux/aarch64,linux/amd64,linux/arm . --push`

## Sources
- https://github.com/dperson/samba
- https://github.com/Mossop/docker-samba
- https://github.com/andrespp/docker-samba-ldap
