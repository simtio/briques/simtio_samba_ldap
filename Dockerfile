#################################
## Dockerfile Samba            ##
## SIMTIO                      ##
#################################

# Get SIMTIO template image
FROM simtio/base_image:latest

# Open requiered ports for samba
EXPOSE 137/udp 138/udp 139 445

# Add filese
COPY files/ entrypoint.sh /

# Install packages
RUN apt-get update \
    && apt-get install --no-install-recommends --no-install-suggests -qy samba smbclient \
    && apt-get clean \
    && rm -rf /tmp/* \
    && rm -rf /var/lib/apt/lists/* \
    && chmod -v +x /entrypoint.sh ;

HEALTHCHECK --interval=60s --timeout=15s CMD smbclient -L '\\localhost' -U '%' -m SMB3

#VOLUME ["/var/cache/samba", "/var/lib/samba", "/var/log/samba", "/run/samba"]

ENTRYPOINT ["/entrypoint.sh"]
